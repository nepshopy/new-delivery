<?php

namespace App\Http\Controllers;

use App\view_count;
use Illuminate\Http\Request;

class ViewCountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\view_count  $view_count
     * @return \Illuminate\Http\Response
     */
    public function show(view_count $view_count)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\view_count  $view_count
     * @return \Illuminate\Http\Response
     */
    public function edit(view_count $view_count)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\view_count  $view_count
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, view_count $view_count)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\view_count  $view_count
     * @return \Illuminate\Http\Response
     */
    public function destroy(view_count $view_count)
    {
        //
    }
}
