-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 10, 2020 at 04:03 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nepshopy_ecomm2020`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `top` int(1) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `logo`, `top`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(1, 'Acer', 'uploads/brands/M9mBFVw1EHA2hGJf4SHQD7dVReFyWbnvJ4QQQgiz.jpeg', 1, 'acer', 'acer', 'all Acer items', '2019-03-12 06:05:56', '2020-08-12 19:37:34'),
(2, 'Hp', 'uploads/brands/5EvztnjmWOsIHjpl5Ye8MdKqWBI3pnxfqsO4qLqa.jpeg', 1, 'hp', 'hp', 'all Hp items', '2019-03-12 06:06:13', '2020-08-12 19:37:19'),
(5, 'Dell', 'uploads/brands/bAkLcBzwEkEDHhV08oYn8Z6OcDZn80sy5JOypOvx.jpeg', 1, 'dell', 'dell', 'all dell items', '2020-07-12 15:53:29', '2020-08-12 19:37:06'),
(6, 'Lg', 'uploads/brands/gadk3FVuPt3ZVCF8FfRuoaZFWaPPxuzYa05FH437.jpeg', 0, 'lg', 'lg', 'all Lg brand', '2020-07-12 15:53:44', '2020-08-12 19:36:50'),
(7, 'Apple', 'uploads/brands/KXo7wFZ1HBYy4bKHbFRl4ZvE2krMprQUHFekZvz1.jpeg', 1, 'apple', 'apple', 'all apple brands', '2020-07-12 15:54:03', '2020-08-12 19:36:25'),
(8, 'Samsung', 'uploads/brands/zbLOKtxeKRavpxEXp8rdfHBX6hPbaBvrbhthsK1k.jpeg', 1, 'samsung', 'samsung', 'all samsung brands', '2020-07-12 15:54:48', '2020-08-12 19:36:06'),
(9, 'sony', 'uploads/brands/4ksvz7ZxIZHKvx1nhzkKYPo3lvdsJRJ9QXPYuqH3.jpeg', 1, 'sony', 'sonyinc', 'sony', '2020-07-12 15:54:58', '2020-08-12 19:35:54'),
(10, 'Asus', 'uploads/brands/55WfAtf8xHjPHGiYVGMFuyfKvE0TkG6nb0SBsjfr.jpeg', 0, 'Asus-BEKOG', 'Asus', 'all Asus item', '2020-08-12 20:13:54', '2020-08-12 20:13:54'),
(11, 'blackberry', 'uploads/brands/GxpN2yX7tJsfDnEFg3MSzWDJ8t20WFyKRWSK59en.jpeg', 0, 'blackberry-LQhvO', 'Blackberry', 'all Blackberry item', '2020-08-12 20:15:23', '2020-08-12 20:15:23'),
(13, 'Bosch', 'uploads/brands/B28aJ49QQFUQFBgLbxijlJX5SQYrtao5BlzGLX2x.jpeg', 0, 'Bosch-gjGHg', 'Bosch', 'all Bosch item', '2020-08-12 20:16:18', '2020-08-12 20:16:18'),
(14, 'Honor', 'uploads/brands/ec4lfS3sXsCgloQQ4D2mm8ZqdKvKlel3NeJK4dT9.jpeg', 0, 'Honor-DAgd8', 'Honor', 'all Honor item', '2020-08-12 20:17:01', '2020-08-12 20:17:01'),
(15, 'Huawei', 'uploads/brands/faQZH1XAKJZjVexIrdfGTY9pVb1lBRwWH42qZcDm.jpeg', 0, 'Huawei-ug6za', 'Huawei', 'all Huawei item', '2020-08-12 20:18:49', '2020-08-12 20:18:49'),
(16, 'Lenovo', 'uploads/brands/uKyJkSZ24HprPYrqUICV3jD9C1z3UyI4kzHgCbna.jpeg', 0, 'Lenovo-VxMMZ', 'Lenovo', 'all Lenovo item', '2020-08-12 20:19:51', '2020-08-12 20:19:51'),
(17, 'Micromax', 'uploads/brands/DFWfRN5zeIUswQo56sXtCFbUVDGTSXTicd9UGAi3.jpeg', 0, 'Micromax-ehqyp', 'Micromax', 'All Micromax item', '2020-08-12 20:20:22', '2020-08-12 20:20:22'),
(18, 'Microsoft', 'uploads/brands/zmcWLhoAaHbSREPXWmUGey3wNe8sIMvALYaW3To8.jpeg', 0, 'Microsoft-1dALm', 'Microsoft', 'all Microsoft items', '2020-08-12 20:21:10', '2020-08-12 20:21:10'),
(19, 'Motorola', 'uploads/brands/qE8v4xbiqQX2XICdo0IyE5ZHT2OcObmpwk1atDsF.jpeg', 0, 'Motorola-6XHEA', 'Motorola', 'all Motorola items', '2020-08-12 20:21:42', '2020-08-12 20:21:42'),
(20, 'Nokia', 'uploads/brands/vlsDMrjhN70CjdCh0qnN4LudSiJAj2pdv1uBNrn8.jpeg', 0, 'Nokia-z8Eg8', 'Nokia', 'all Nokia items', '2020-08-12 20:22:20', '2020-08-12 20:22:20'),
(21, 'Oneplus', 'uploads/brands/eXphF9ZL5v2rm2x7ptt0Bui0XvJBe8Qhs2nAx7iQ.jpeg', 0, 'Oneplus-qyjsM', 'Oneplus', 'all Oneplus item', '2020-08-12 20:22:57', '2020-08-12 20:22:57'),
(22, 'Panasonic', 'uploads/brands/lBbgXrrW6S3Bm1Tv6UpNMGnGnpxQtiTZeZaXE5pd.jpeg', 0, 'Panasonic-9Yyah', 'Panasonic', 'all Panasonic item', '2020-08-12 20:23:40', '2020-08-12 20:23:40'),
(24, 'Xiaomi', 'uploads/brands/bLZzXT668IPNsjlgER32awHUVrpnuTEmCbTvACVe.jpeg', 0, 'Xiaomi-eSp0L', 'Xiaomi', 'all Xiaomi items', '2020-08-12 20:24:38', '2020-08-12 20:24:38'),
(25, 'Vivo', 'uploads/brands/6XuD45PAjC3MEXzjs4vibyiaz6xEbrtOPdfAiMpj.jpeg', 0, 'Vivo-0rPEA', 'VIvo', 'all Vivo item', '2020-08-12 20:25:58', '2020-08-12 20:25:58'),
(26, 'Vodafone', 'uploads/brands/imZV7mH8C29xnlBrM9IIXJ6dzMSOlDqUByZjFOCz.jpeg', 0, 'Vodafone-QEinn', 'Vodafone', 'all Vodafone item', '2020-08-12 20:26:25', '2020-08-12 20:26:25'),
(27, 'Beats', 'uploads/brands/zs2U0KFZnEP4giY6Fa1CLWyK08m4lXGcA0FoZBPS.jpeg', 0, 'Beats-hXNeM', 'Beats', 'all Beats item', '2020-08-12 20:50:37', '2020-08-12 20:50:37'),
(29, 'Canon', 'uploads/brands/3GXX8ajscFXmiNdcVvw6qPpLk2DUxkl6spkryO1v.jpeg', 0, 'Canon-qJPBT', 'Canon', 'all Canon item', '2020-08-12 20:51:02', '2020-08-12 20:51:02'),
(30, 'GoPro', 'uploads/brands/2MIuqTDEmwXmgoBVhT96Tihoq47w35eYbkFaMkzn.jpeg', 0, 'GoPro-HxylM', 'GoPro', 'all GoPro item', '2020-08-12 20:51:48', '2020-08-12 20:51:48'),
(31, 'Nikon', 'uploads/brands/rPx1YeLmazmEFSDutrHhqWLlWOxlmocpbD2NpItP.jpeg', 0, 'Nikon-dMmeG', 'Nikon', 'all Nikon item', '2020-08-12 20:52:50', '2020-08-12 20:52:50'),
(32, 'SanDisk', 'uploads/brands/PXqyE0J9NwL6DexsijqaSZk0LWsQaFRWG8bUGZp3.jpeg', 0, 'SanDisk-Xklmz', 'SanDisk', 'all SanDisk item', '2020-08-12 20:53:36', '2020-08-12 20:53:36'),
(33, 'Rebok', 'uploads/brands/kgRvozIGcZX3HIGjWkoco23uXndfklFWrMjuCfjt.jpeg', 0, 'Rebok-cY1dv', 'Rebok', 'all Rebok item', '2020-08-12 21:05:55', '2020-08-12 21:05:55'),
(34, 'D & G', 'uploads/brands/kGQGO8zVHERwzFFS8nzMyMancAenUIMBqXtkwzLw.jpeg', 0, 'D--G-hWRcO', 'D & G', 'D & G items', '2020-08-12 21:30:39', '2020-08-12 21:30:39'),
(35, 'vans', 'uploads/brands/rU01B0CgfOU9FUE3BWjsiUP7h3AF00SSOXhm0hnx.jpeg', 0, 'vans-LK4qB', 'Vans', 'Vans item', '2020-08-12 21:31:00', '2020-08-12 21:31:00'),
(36, 'Louis Vuitton', 'uploads/brands/4UU2dDySt1VTAm921sFX6o1LCm2vXyRv09Xq35Ca.jpeg', 0, 'Louis-Vuitton-kLOte', 'Louis Vuitton', 'Louis Vuitton items', '2020-08-12 21:32:22', '2020-08-12 21:32:22'),
(37, 'Under Armour', 'uploads/brands/JUpvFzhZ6fyhVVpHDW0eA8g0ucWWW2kSwMVQVoVB.jpeg', 0, 'Under-Armour-T2ON3', 'Under Armour', 'Under Armour items', '2020-08-12 21:33:12', '2020-08-12 21:33:12'),
(38, 'Mizuno', 'uploads/brands/5rUFr8hXsBjiwJgUEBAKzMndwd71DyFfc0MZf2SE.jpeg', 0, 'Mizuno-MX1fg', 'Mizuno', 'Mizuno Items', '2020-08-12 21:34:06', '2020-08-12 21:34:06'),
(39, 'Fila', 'uploads/brands/Gkwqmx60DawshCeNVNMXsr2YBlDhecFqeBK6y692.jpeg', 0, 'Fila-7ol9b', 'Fila', 'Fila items', '2020-08-12 21:36:29', '2020-08-12 21:36:29'),
(40, 'Champion', 'uploads/brands/jhzxI4naz2cxD2wSrKlomZb53WMY0lavHR0adQjw.jpeg', 0, 'Champion-dHatF', 'Champion', 'Champion items', '2020-08-12 21:37:16', '2020-08-12 21:37:16'),
(41, 'ADIDAS', 'uploads/brands/M5hwWeo36NAv2KoWuF5xxcfOAwzUaMSrQucRfnWU.jpeg', 0, 'ADIDAS-UD1Jw', 'ADIDAS', 'ADIDAS items', '2020-08-12 21:37:51', '2020-08-12 21:37:51'),
(42, 'Gucci', 'uploads/brands/yLrrjtCGn1xLPL1Asing1ZnnFupE8CwE3WSyraS9.jpeg', 0, 'Gucci-IuBfp', 'Gucci', 'Gucci items', '2020-08-12 21:38:24', '2020-08-12 21:38:24'),
(43, 'Levis', 'uploads/brands/Lr1IwtKKM5C8scbZVeZPpr28GpUT7B6EfAf7wicd.jpeg', 0, 'Levis-4H4za', 'levis', 'Levis item', '2020-08-12 21:39:29', '2020-08-12 21:39:29'),
(44, 'Erke', 'uploads/brands/h6EZj8QSP0BAoHVCqAk4HUvr3ftPHtUY5RTVfSK9.jpeg', 0, 'Erke-PVS8L', 'Erke', 'Erke items', '2020-08-12 21:39:51', '2020-08-12 21:39:51'),
(45, 'Nike', 'uploads/brands/Z9T4QfewgdqKfYDjf3eQsaCouYxcIreTQjxGpgeO.jpeg', 0, 'Nike-4GmxS', 'Nike', 'Nike items', '2020-08-12 21:40:24', '2020-08-12 21:40:24'),
(46, 'Puma', 'uploads/brands/rs7ZTlTWmKZWnwtgkTgsEc5DleAiHtMNOtsEtbkU.jpeg', 0, 'Puma-23W0u', 'Puma', 'Puma items', '2020-08-12 21:40:57', '2020-08-12 21:40:57'),
(47, 'Converse', 'uploads/brands/vrCXWlWvZf4hs2d2AliqxFd4vBS61VlASxk7IvsZ.jpeg', 0, 'Converse-kDKjh', 'Converse', 'Converse item', '2020-08-12 21:41:44', '2020-08-12 21:41:44'),
(48, 'Lee', 'uploads/brands/bipBZecPGxHWJar87y1gErOXqJ11XQUgnX7VgfcH.jpeg', 0, 'Lee-6Odh7', 'Lee', 'Lee items', '2020-08-12 21:42:05', '2020-08-12 21:42:23'),
(49, 'Boss', 'uploads/brands/IyLgDYQ3cXldB9KZn9bmvVOu19e8Er9tamlcjSsw.jpeg', 0, 'Boss-0DmZq', 'Boss', 'Boss items', '2020-08-12 21:43:25', '2020-08-12 21:43:25'),
(50, 'Lacoste', 'uploads/brands/qwxSb5yrWl3nQE5FBDvg6zA2NBCd01vnN5hSm78T.jpeg', 0, 'Lacoste-RMG93', 'Lacoste', 'Lacoste items', '2020-08-12 21:44:04', '2020-08-12 21:44:20'),
(51, 'Zara', 'uploads/brands/OXdhz2xWVnfthNzpAdOgSLZazPZTqEOn4mTQnIUz.jpeg', 0, 'Zara-1UFnK', 'Zara', 'Zara items', '2020-08-12 21:44:41', '2020-08-12 21:44:41'),
(52, 'Bata', 'uploads/brands/juqh4NvASczH0wZf87sjwp6dSyBXj7ATbIqiZRxg.jpeg', 0, 'Bata-lWRFZ', 'bata', 'Bata items', '2020-08-12 21:45:08', '2020-08-12 21:45:08'),
(53, 'Diesel', 'uploads/brands/hnGvqMkXIIqeE8j6LT9CkOKPoTREhOEunq0FSPV9.jpeg', 0, 'Diesel-fyDZI', 'Diesel', 'Diesel items', '2020-08-12 21:45:54', '2020-08-12 21:45:54'),
(54, 'Giorgio Armani', 'uploads/brands/anbhoPocDIUcT7g8MC1gtQ8c6jdlHZbypCln40g5.jpeg', 0, 'Giorgio-Armani-6LpLY', 'Giorgio Armani', 'Giorgio Armani items', '2020-08-12 21:47:05', '2020-08-12 21:47:05'),
(55, '361', 'uploads/brands/qTX8oNJrOg23NABn5aS2L5CIcj2KNrlhu3W3SEWR.jpeg', 0, '361-CXWJA', '361', '361 items', '2020-08-12 21:49:03', '2020-08-12 21:49:03'),
(56, 'Endura', 'uploads/brands/0yEqaq2yJfYkYxcnIbxPEU4zbYmp3pFe8HvLu66q.jpeg', 0, 'Endura-i6Zmc', 'Endura', 'Endura items', '2020-08-12 21:49:41', '2020-08-12 21:49:41'),
(57, 'Crizal', 'uploads/brands/WcL8vMKmyba3Gzs9WjgR9NTipe0PAZS32vl3rxlO.jpeg', 0, 'Crizal-2DFyx', 'Crizal', 'Crizal items', '2020-08-13 19:48:02', '2020-08-13 19:48:02'),
(58, 'Hero', 'uploads/brands/0gthuFz324txIT6IbE1kPtF29zsCZPSpSz5FEsyd.jpeg', 0, 'Hero-msLeZ', 'Hero', 'Hero items', '2020-08-13 20:05:18', '2020-08-13 20:05:18'),
(59, 'Timberland', 'uploads/brands/PN98YdOidHQJ3NJBUyyFsRvFAtHnaDk5p7Z8ISoo.jpeg', 0, 'Timberland-R4FHe', 'Timberland', 'Timberland products', '2020-08-13 20:05:53', '2020-08-13 20:05:53'),
(60, 'Benq', 'uploads/brands/o91ikVmOykl05jvWD0oPVCTycKWb0gYhceGSTR2b.jpeg', 0, 'Benq-JBkol', 'Benq', 'Benq products', '2020-08-13 20:06:21', '2020-08-13 20:06:21'),
(61, 'Dhikhar', 'uploads/brands/QfwCt5RSCsPYSErlFeHo9X5znm8L04TlW4jscLNB.jpeg', 0, 'Dhikhar-XLMHF', 'Shikhar', 'Shikhar products', '2020-08-13 20:06:53', '2020-08-13 20:06:53'),
(62, 'Seiko', 'uploads/brands/OgNfm2V9VtnOKoOZBSXBnVQS7JtwzIgwwT1RbowW.jpeg', 0, 'Seiko-RNGjJ', 'Seiko', 'Seiko products', '2020-08-13 20:07:18', '2020-08-13 20:07:32'),
(63, 'Rolex', 'uploads/brands/uC6o9Qa7UfHSE87eqG944pSrXTdL9x59jmo2sDZH.jpeg', 0, 'Rolex-rU6gV', 'Rolex', 'Rolex products', '2020-08-13 20:08:06', '2020-08-13 20:08:06'),
(64, 'Sonata', 'uploads/brands/wloi8sVDu6Nvsos78p7khNT4weaEvQz2bQg9FxtF.jpeg', 0, 'Sonata-pX1IZ', 'Sonata', 'Sonata products', '2020-08-13 20:08:39', '2020-08-13 20:08:39'),
(65, 'Hilife', 'uploads/brands/71Qg0COsfeskjJVxuIDWbcqe2w78l0oBLkhzDmPh.jpeg', 0, 'Hilife-oxmEu', 'Hilife', 'Hilife Product', '2020-08-13 20:09:14', '2020-08-13 20:09:14'),
(66, 'Goldstar', 'uploads/brands/rYmSAYYNJ6IZIzSNUzVF06jrVwvFN8Nb09cTU4mM.jpeg', 0, 'Goldstar-Qydyl', 'Goldstar', 'Goldstar product', '2020-08-13 20:11:16', '2020-08-13 20:11:16'),
(67, 'SMK Helmets', 'uploads/brands/0MG9zfJAYMeCjU9ace2t5di7K20PmyIBO2lMTG6c.jpeg', 0, 'SMK-Helmets-FC6ta', 'SMK Helmets', 'SMK Helmets PRODUCTS', '2020-08-13 20:11:59', '2020-08-13 20:11:59'),
(68, 'Royal', 'uploads/brands/raIGZvpAL25J3I4q7AK1f1PnesAhe5dSCQy8OKKL.jpeg', 0, 'Royal-fMYJQ', 'Royal', 'Royal products', '2020-08-13 20:13:02', '2020-08-13 20:13:02'),
(69, 'Godrej', 'uploads/brands/WhEx8JGEe5tGyQIINJANrEhUHVphaK8eTYPN2kHn.jpeg', 0, 'Godrej-KOudb', 'Godrej', 'Godrej product', '2020-08-13 20:13:36', '2020-08-13 20:13:36'),
(70, 'Raymond', 'uploads/brands/fvsSDT8aUrgLysHAblfvF9awb1pXR5RM43CtbFyQ.jpeg', 0, 'Raymond-KewAH', 'Raymond', 'Raymond product', '2020-08-13 20:14:25', '2020-08-13 20:14:25'),
(71, 'Vega', 'uploads/brands/UUJzD6Kp9l8FpoCYOBNMNu6kXxQ4smOQbaJkoSfT.jpeg', 0, 'Vega-OMod4', 'Vega', 'Vega products', '2020-08-13 20:15:02', '2020-08-13 20:15:02'),
(72, 'Arai', 'uploads/brands/hlapjaWjfM3eYr6zEhl2ICTv1mD5MZNYFmDdRyrV.jpeg', 0, 'Arai-quzRE', 'Arai', 'Arai products', '2020-08-13 20:15:38', '2020-08-13 20:15:38'),
(73, 'KTM CITY', 'uploads/brands/65A2gqjV85nHoJL0JkA5tbe0gqVkVO4eHzzS9yjj.jpeg', 0, 'KTM-CITY-yKu7q', 'KTM CITY', 'KTM CITY products', '2020-08-13 20:16:12', '2020-08-13 20:16:12'),
(74, 'Ray Ban', 'uploads/brands/GbCPupzWNXQ1eNueMpyon9jND2e5n2nRiwGtEPXN.jpeg', 0, 'Ray-Ban-8YgSP', 'Ray Ban', 'Ray Ban Products', '2020-08-13 20:16:37', '2020-08-13 20:16:37'),
(75, 'Hitachi', 'uploads/brands/RhRbFP6s0zrSpfto3AUo1suIa212k5dSgOWFY465.jpeg', 0, 'Hitachi-FD9GB', 'Hitachi', 'Hitachi products', '2020-08-13 20:17:02', '2020-08-13 20:17:02'),
(76, 'Yahama', 'uploads/brands/0eoAPrcYhlLnitOH1apCOe4sT721cFjR415TVVoj.jpeg', 0, 'Yahama-oUjQc', 'Yahama', 'Yahama Products', '2020-08-13 20:18:20', '2020-08-13 20:18:20'),
(77, 'Bajaj', 'uploads/brands/VTuIRIQ5ziHXepeW3WqtfBa0c9aj2nU9V0ntr1Mz.jpeg', 0, 'Bajaj-pZ80w', 'bajaj', 'Bajaj products', '2020-08-13 20:19:00', '2020-08-13 20:19:00'),
(78, 'wirlpool', 'uploads/brands/aeBUxg3hzyYxKdBDIsOil7g9N8UH17CyHSquLh9S.jpeg', 0, 'wirlpool-lWqO0', 'Wirlpool', 'Wirlpool Products', '2020-08-13 20:24:40', '2020-08-13 20:24:40'),
(79, 'Baltra', 'uploads/brands/vw8qCljjU8kzRYFLw62MAKCZlIdot4v6sQz4amO0.jpeg', 0, 'Baltra-mehxq', 'Baltra', 'baltra ptoducts', '2020-08-13 20:25:11', '2020-08-13 20:25:11'),
(80, 'Mammut', 'uploads/brands/3eHdGZtmqfC438cp5318FN9IZ7Ro2Kjr12D3XFqh.jpeg', 0, 'Mammut-qPfOS', 'Mammut', 'Mammut  product', '2020-08-13 20:26:47', '2020-08-13 20:26:47'),
(81, 'The North Face', 'uploads/brands/g3cuR5RXtPItbA2dgEjDchKQNbjztaPAv3HDM6XY.jpeg', 0, 'The-North-Face-G36qP', 'The North Face', 'The North Face', '2020-08-13 20:27:22', '2020-08-13 20:27:22'),
(82, 'Caliber', 'uploads/brands/E91DIdoX1b304yvaLjDfRgNdoYwzxAzDjsdWP44Z.jpeg', 0, 'Caliber-JwonN', 'Caliber', 'Caliber products', '2020-08-13 20:27:53', '2020-08-13 20:27:53'),
(84, 'Colors', NULL, 0, 'Colors-nhHbM', NULL, NULL, '2020-09-02 23:32:44', '2020-09-02 23:32:44'),
(85, 'MSI', NULL, 0, 'MSI-Ezh9t', NULL, NULL, '2020-09-03 17:24:21', '2020-09-03 17:24:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
