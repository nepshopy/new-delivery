-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 11, 2020 at 01:45 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nepshopy_food`
--

-- --------------------------------------------------------

--
-- Table structure for table `addons`
--

CREATE TABLE `addons` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `addon_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `addons`
--

INSERT INTO `addons` (`term_id`, `addon_id`, `created_at`, `updated_at`) VALUES
(34, 25, '2020-08-26 23:54:59', '2020-08-26 23:54:59'),
(34, 26, '2020-08-26 23:54:59', '2020-08-26 23:54:59'),
(34, 29, '2020-08-26 23:54:59', '2020-08-26 23:54:59'),
(34, 30, '2020-08-26 23:54:59', '2020-08-26 23:54:59'),
(34, 33, '2020-08-26 23:54:59', '2020-08-26 23:54:59'),
(35, 25, '2020-08-26 23:56:23', '2020-08-26 23:56:23'),
(35, 26, '2020-08-26 23:56:23', '2020-08-26 23:56:23'),
(35, 29, '2020-08-26 23:56:23', '2020-08-26 23:56:23'),
(35, 32, '2020-08-26 23:56:23', '2020-08-26 23:56:23'),
(37, 36, '2020-08-27 00:06:41', '2020-08-27 00:06:41'),
(38, 36, '2020-08-27 00:07:39', '2020-08-27 00:07:39');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `content` text COLLATE utf8mb4_unicode_ci,
  `p_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `avatar`, `type`, `content`, `p_id`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Uncategorizied', 'uncategorizied', NULL, '0', NULL, NULL, 0, 1, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(2, 'Indian', 'indian', 'http://food.chitosewa.com/uploads/dummy/cuisine/indian.jpg', '2', NULL, NULL, 0, 1, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(3, 'Chinese food', 'chinese-food', 'http://food.chitosewa.com/uploads/dummy/cuisine/chinese.jpg', '2', NULL, NULL, 0, 1, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(4, 'Japanese', 'japanese-food', 'http://food.chitosewa.com/uploads/dummy/cuisine/japanese.jpg', '2', NULL, NULL, 0, 1, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(5, 'Italian', 'italian', 'http://food.chitosewa.com/uploads/dummy/cuisine/italian.jpg', '2', NULL, NULL, 0, 1, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(6, 'mexican', 'mexican', 'http://food.chitosewa.com/uploads/dummy/cuisine/mexican.jpg', '2', NULL, NULL, 0, 1, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(7, 'Appetizer', 'appetizer', NULL, '1', NULL, NULL, 0, 1, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(8, 'Salad', 'salad', NULL, '1', NULL, NULL, 0, 1, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(9, 'Soup', 'soup', NULL, '1', NULL, NULL, 0, 1, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(10, 'Rice Dish', 'rice', NULL, '1', NULL, NULL, 0, 1, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(11, 'Chicken Dish', 'rice', NULL, '1', NULL, NULL, 0, 1, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(12, 'Fish Dish', 'rice', NULL, '1', NULL, NULL, 0, 1, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(13, 'Mutton Dish', 'rice', NULL, '1', NULL, NULL, 0, 1, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(14, 'Newari', 'newari', '//food.chitosewa.com/uploads/20/08/26082015984245205f4605c823bc5.jpg', '2', NULL, NULL, 0, 1, '2020-08-26 12:48:48', '2020-08-26 12:48:48'),
(15, 'Nepali', 'nepali', '//food.chitosewa.com/uploads/20/08/26082015984246015f4606194131f.jpg', '2', NULL, NULL, 0, 1, '2020-08-26 12:50:08', '2020-08-26 12:50:08'),
(16, 'Pizza', NULL, NULL, '1', NULL, NULL, 0, 14, '2020-08-26 23:33:34', '2020-08-26 23:33:34'),
(20, 'Pasta', NULL, NULL, '1', NULL, NULL, 0, 14, '2020-08-26 23:34:20', '2020-08-26 23:34:20'),
(21, 'MOMO', NULL, NULL, '1', NULL, NULL, 0, 14, '2020-08-26 23:34:25', '2020-08-26 23:34:25'),
(23, 'Veg Pizza', NULL, NULL, '1', NULL, 16, 0, 14, '2020-08-26 23:47:44', '2020-08-26 23:47:44'),
(24, 'Non Veg. Pizza', NULL, NULL, '1', NULL, 16, 0, 14, '2020-08-26 23:48:11', '2020-08-26 23:48:11'),
(25, 'Buff Piza', NULL, NULL, '1', NULL, 16, 0, 14, '2020-08-26 23:52:03', '2020-08-26 23:52:03'),
(26, 'Veg MoMo', NULL, NULL, '1', NULL, 21, 0, 14, '2020-08-26 23:52:24', '2020-08-26 23:52:24'),
(27, 'Chicken MoMo', NULL, NULL, '1', NULL, 21, 0, 14, '2020-08-26 23:52:40', '2020-08-26 23:52:40'),
(28, 'Buff MoMo', NULL, NULL, '1', NULL, 21, 0, 14, '2020-08-26 23:52:52', '2020-08-26 23:52:52'),
(29, 'momos', NULL, NULL, '1', NULL, NULL, 0, 18, '2020-09-10 17:09:03', '2020-09-10 17:09:03'),
(30, 'pizza', NULL, NULL, '1', NULL, NULL, 0, 18, '2020-09-10 17:09:11', '2020-09-10 17:09:11');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `vendor_id`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 15, 14, 1, '2020-08-26 20:55:43', '2020-08-26 20:55:43');

-- --------------------------------------------------------

--
-- Table structure for table `comment_meta`
--

CREATE TABLE `comment_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` bigint(20) UNSIGNED NOT NULL,
  `star_rate` int(11) NOT NULL DEFAULT '0',
  `comment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comment_meta`
--

INSERT INTO `comment_meta` (`id`, `comment_id`, `vendor_id`, `star_rate`, `comment`, `created_at`, `updated_at`) VALUES
(1, 1, 14, 5, 'Good Service', '2020-08-26 20:55:43', '2020-08-26 20:55:43');

-- --------------------------------------------------------

--
-- Table structure for table `customizers`
--

CREATE TABLE `customizers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `theme_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customizers`
--

INSERT INTO `customizers` (`id`, `key`, `theme_name`, `value`, `status`, `created_at`, `updated_at`) VALUES
(1, 'hero', 'khana', '{\"settings\":{\"hero_right_image\":{\"old_value\":null,\"new_value\":\"uploads\\/2020-08-03-5f27e155e25e4.jpg\"},\"hero_right_title\":{\"old_value\":\"Hello Usa The Best 20% off\",\"new_value\":\"Get 20% Off From Special Day\"},\"hero_title\":{\"old_value\":\"Find Awesome Deals in Bangladesh\",\"new_value\":\"Find Awesome Deals in Nepal\"},\"hero_des\":{\"old_value\":\"Lists of top restaurants, cafes, pubs and bars in Melbourne, based on trends\",\"new_value\":\"Lists of top restaurants, cafes, pubs and bars in Kathmandu, based on trends\"},\"button_title\":{\"old_value\":null,\"new_value\":\"Search\"},\"offer_title\":{\"old_value\":null,\"new_value\":\"Available Offer Right Now\"},\"hero_right_content\":{\"old_value\":null,\"new_value\":\"VALID ON SELECT ITEM\"}}}', '1', '2020-08-26 11:34:02', '2020-08-26 12:12:45'),
(2, 'header', 'khana', '{\"settings\":{\"logo\":{\"old_value\":\"uploads\\/2020-08-26-5f462382b52ea.png\",\"new_value\":\"uploads\\/2020-08-26-5f4639a7f0826.png\"},\"header_pn\":{\"old_value\":\"+825-285-9687\",\"new_value\":\"+977-9801621134\"},\"rider_team_title\":{\"old_value\":\"Join Our Khana Rider Team!\",\"new_value\":\"Join Our Chitosewa Rider Team!\"},\"rider_button_title\":{\"old_value\":null,\"new_value\":\"Apply Now\"}}}', '1', '2020-08-26 11:34:02', '2020-08-26 16:30:03'),
(3, 'category', 'khana', '{\"settings\":{\"category_title\":{\"old_value\":null,\"new_value\":\"Browse By Category\"}}}', '1', '2020-08-26 11:34:02', '2020-08-26 11:34:02'),
(4, 'best_restaurant', 'khana', '{\"settings\":{\"best_restaurant_title\":{\"old_value\":null,\"new_value\":\"Best Rated Restaurant\"}}}', '1', '2020-08-26 11:34:02', '2020-08-26 11:34:02'),
(5, 'city_area', 'khana', '{\"settings\":{\"find_city_title\":{\"old_value\":null,\"new_value\":\"Find us in these cities and many more!\"}}}', '1', '2020-08-26 11:34:02', '2020-08-26 11:34:02'),
(6, 'featured_resturent', 'khana', '{\"settings\":{\"featured_resturent_title\":{\"old_value\":null,\"new_value\":\"Featured Restaturents\"}}}', '1', '2020-08-26 11:34:02', '2020-08-26 11:34:02'),
(7, 'footer', 'khana', '{\"settings\":{\"copyright_area\":{\"old_value\":\"\\u00a9 Copyright 2020 Amcoders. All rights reserved\",\"new_value\":\"\\u00a9 Copyright 2020 Chitosewa. All rights reserved\"}}}', '1', '2020-08-26 11:34:02', '2020-08-26 12:18:33');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `featured_user`
--

CREATE TABLE `featured_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `live`
--

CREATE TABLE `live` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `latitute` double DEFAULT NULL,
  `longlatitute` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `live`
--

INSERT INTO `live` (`id`, `order_id`, `latitute`, `longlatitute`, `created_at`, `updated_at`) VALUES
(1, 1, 27.6631429, 85.3392644, '2020-08-26 20:21:10', '2020-08-26 20:21:10'),
(2, 2, 27.6631416, 85.3392682, '2020-08-26 21:06:29', '2020-08-26 21:10:44');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL DEFAULT '3',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `user_id`, `term_id`, `latitude`, `longitude`, `role_id`, `created_at`, `updated_at`) VALUES
(11, 13, 18, 0, 0, 3, '2020-08-26 12:23:55', '2020-08-26 12:23:55'),
(12, 14, 18, 27.68864816824, 85.2861470640625, 3, '2020-08-26 22:55:41', '2020-08-26 20:50:14'),
(13, 16, 18, 27.6862181, 85.3149142, 4, '2020-08-27 00:14:08', '2020-08-26 21:08:00'),
(14, 18, 18, 27.678775, 85.3496333, 3, '2020-09-10 16:45:54', '2020-09-10 17:42:37');

-- --------------------------------------------------------

--
-- Table structure for table `medias`
--

CREATE TABLE `medias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `medias`
--

INSERT INTO `medias` (`id`, `name`, `type`, `url`, `size`, `path`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'uploads/20/08/26082015984213475f45f963c6f47.jpg', 'jpg', '//food.chitosewa.com/uploads/20/08/26082015984213475f45f963c6f47.jpg', '21972kib', 'uploads/20/08/', 1, '2020-08-26 11:55:47', '2020-08-26 11:55:47'),
(2, 'uploads/20/08/26082015984217645f45fb04b52ef.jpg', 'jpg', '//food.chitosewa.com/uploads/20/08/26082015984217645f45fb04b52ef.jpg', '51158kib', 'uploads/20/08/', 1, '2020-08-26 12:02:44', '2020-08-26 12:02:44'),
(3, 'uploads/20/08/26082015984221885f45fcac6bc7b.jpg', 'jpeg', '//food.chitosewa.com/uploads/20/08/26082015984221885f45fcac6bc7b.jpg', '8832kib', 'uploads/20/08/', 1, '2020-08-26 12:09:48', '2020-08-26 12:09:48'),
(4, 'uploads/2020-08-26-5f45fdd81f5fc.png', 'png', '//food.chitosewa.com/uploads/2020-08-26-5f45fdd81f5fc.png', '8866kib', 'uploads/', 1, '2020-08-26 12:14:48', '2020-08-26 12:14:48'),
(5, 'uploads/20/08/26082015984233145f460112efc0a.jpg', 'jpeg', '//food.chitosewa.com/uploads/20/08/26082015984233145f460112efc0a.jpg', '9647kib', 'uploads/20/08/', 1, '2020-08-26 12:28:35', '2020-08-26 12:28:35'),
(6, 'uploads/20/08/26082015984234055f46016d34575.jpg', 'jpg', '//food.chitosewa.com/uploads/20/08/26082015984234055f46016d34575.jpg', '23794kib', 'uploads/20/08/', 1, '2020-08-26 12:30:05', '2020-08-26 12:30:05'),
(7, 'uploads/2020-08-26-5f46042e9f709.png', 'png', '//food.chitosewa.com/uploads/2020-08-26-5f46042e9f709.png', '3823kib', 'uploads/', 1, '2020-08-26 12:41:50', '2020-08-26 12:41:50'),
(8, 'uploads/20/08/26082015984245205f4605c823bc5.jpg', 'jpg', '//food.chitosewa.com/uploads/20/08/26082015984245205f4605c823bc5.jpg', '104202kib', 'uploads/20/08/', 1, '2020-08-26 12:48:40', '2020-08-26 12:48:40'),
(9, 'uploads/20/08/26082015984246015f4606194131f.jpg', 'jpeg', '//food.chitosewa.com/uploads/20/08/26082015984246015f4606194131f.jpg', '11218kib', 'uploads/20/08/', 1, '2020-08-26 12:50:01', '2020-08-26 12:50:01'),
(10, 'uploads/2020-08-26-5f462382b52ea.png', 'png', '//food.chitosewa.com/uploads/2020-08-26-5f462382b52ea.png', '3824kib', 'uploads/', 1, '2020-08-26 14:55:30', '2020-08-26 14:55:30'),
(11, 'uploads/2020-08-26-5f4639a7f0826.png', 'png', '//food.chitosewa.com/uploads/2020-08-26-5f4639a7f0826.png', '3128kib', 'uploads/', 1, '2020-08-26 16:29:59', '2020-08-26 16:29:59'),
(12, 'uploads/20/08/26082015984380325f463a90815f3.jpg', 'jpg', '//food.chitosewa.com/uploads/20/08/26082015984380325f463a90815f3.jpg', '58052kib', 'uploads/20/08/', 1, '2020-08-26 16:33:52', '2020-08-26 16:33:52'),
(13, 'uploads/20/08/26082015984428495f464d61d3b60.jpg', 'jpg', '//food.chitosewa.com/uploads/20/08/26082015984428495f464d61d3b60.jpg', '35584kib', 'uploads/20/08/', 14, '2020-08-26 23:39:09', '2020-08-26 23:39:09'),
(14, 'uploads/20/08/26082015984437855f46510982737.jpg', 'jpg', '//food.chitosewa.com/uploads/20/08/26082015984437855f46510982737.jpg', '27777kib', 'uploads/20/08/', 14, '2020-08-26 23:54:45', '2020-08-26 23:54:45'),
(15, 'uploads/20/08/26082015984438485f465148cc482.jpg', 'jpg', '//food.chitosewa.com/uploads/20/08/26082015984438485f465148cc482.jpg', '37979kib', 'uploads/20/08/', 14, '2020-08-26 23:55:48', '2020-08-26 23:55:48'),
(16, 'uploads/20/08/5f4653c5c7fa52608201598444485.png', 'png', '//food.chitosewa.com/uploads/20/08/5f4653c5c7fa52608201598444485.png', '352999kib', 'uploads/20/08/', 14, '2020-08-27 00:06:28', '2020-08-27 00:06:28'),
(17, 'uploads/20/08/5f4654020887d2608201598444546.png', 'png', '//food.chitosewa.com/uploads/20/08/5f4654020887d2608201598444546.png', '355793kib', 'uploads/20/08/', 14, '2020-08-27 00:07:28', '2020-08-27 00:07:28'),
(18, 'uploads/20/08/26082015984521305f4671a26610d.jpg', 'jpg', '//food.chitosewa.com/uploads/20/08/26082015984521305f4671a26610d.jpg', '31098kib', 'uploads/20/08/', 14, '2020-08-26 20:28:50', '2020-08-26 20:28:50'),
(19, 'uploads/20/08/5f46766b98dbb2608201598453355.png', 'png', '//food.chitosewa.com/uploads/20/08/5f46766b98dbb2608201598453355.png', '4227970kib', 'uploads/20/08/', 14, '2020-08-26 20:49:30', '2020-08-26 20:49:30'),
(20, 'uploads/20/09/10092015997156495f59b941f29f6.jpg', 'jpg', '//chitosewa.com/uploads/20/09/10092015997156495f59b941f29f6.jpg', '65523kib', 'uploads/20/09/', 18, '2020-09-10 17:12:30', '2020-09-10 17:12:30');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `position`, `data`, `lang`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Header', 'Header', '[{\"text\":\"Home\",\"href\":\"/\",\"icon\":\"\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"City\",\"href\":\"#\",\"icon\":\"fas fa-angle-down\",\"target\":\"_self\",\"title\":\"\",\"children\":[{\"text\":\"Kathmandu\",\"href\":\"/area/Kathmandu\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Hetauda\",\"href\":\"/area/Hetauda\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Biratnagar\",\"href\":\"/area/biratnagar\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Janakpur\",\"href\":\"/area/janakpur\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Lahan\",\"href\":\"/area/lahan\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Birganj\",\"href\":\"/area/birganj\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Narayan Ghat\",\"href\":\"/area/narayanghat\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Bharatpur\",\"href\":\"/area/bharatpur\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"}]},{\"text\":\"Customer Area\",\"href\":\"#\",\"icon\":\"fas fa-angle-down\",\"target\":\"_self\",\"title\":\"\",\"children\":[{\"text\":\"Login\",\"href\":\"/user/login\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Register\",\"href\":\"/user/register\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Customer Panel\",\"href\":\"/author/dashboard\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"}]},{\"text\":\"Rider Area\",\"href\":\"#\",\"icon\":\"fas fa-angle-down\",\"target\":\"_self\",\"title\":\"\",\"children\":[{\"text\":\"Login\",\"href\":\"/rider/login\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Register\",\"href\":\"/rider/register\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Rider Panel\",\"href\":\"/rider/dashboard\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"}]},{\"text\":\"Restaurant Area\",\"href\":\"#\",\"icon\":\"fas fa-angle-down\",\"target\":\"_self\",\"title\":\"\",\"children\":[{\"text\":\"Login\",\"href\":\"/login\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Register\",\"href\":\"/restaurant/register\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"Restaurant Panel\",\"href\":\"/store/dashboard\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"}]}]', 'en', 1, '2020-08-26 11:34:02', '2020-08-26 12:25:45'),
(2, 'Header', 'Header', '[{\"text\":\"হোম\",\"href\":\"/\",\"icon\":\"\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"আপানার শহর\",\"href\":\"#\",\"icon\":\"fas fa-angle-down\",\"target\":\"_self\",\"title\":\"\",\"children\":[{\"text\":\"ঢাকা\",\"href\":\"/area/dhaka\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"চট্রগ্রাম\",\"href\":\"/area/chittagong\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\" ফেণী\",\"href\":\"/area/feni\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"বগুড়া\",\"href\":\"/area/bogra\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"বরিশাল\",\"href\":\"/area/barisal\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"রাজশাহী\",\"href\":\"/area/rajshahi\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"খুলনা\",\"href\":\"/area/khulna\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"রংপুর\",\"href\":\"/area/rangpur\",\"icon\":\"empty\",\"target\":\"_self\",\"title\":\"\"}]},{\"text\":\"ক্রেতা\",\"icon\":\"fas fa-angle-down\",\"href\":\"#\",\"target\":\"_self\",\"title\":\"\",\"children\":[{\"text\":\"লগইন\",\"icon\":\"empty\",\"href\":\"/user/login\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"রেজিস্টার\",\"icon\":\"empty\",\"href\":\"/user/register\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"ড্যাশবোর্ড\",\"icon\":\"empty\",\"href\":\"/author/dashboard\",\"target\":\"_self\",\"title\":\"\"}]},{\"text\":\"রাইডার\",\"icon\":\"fas fa-angle-down\",\"href\":\"#\",\"target\":\"_self\",\"title\":\"\",\"children\":[{\"text\":\"লগইন\",\"icon\":\"empty\",\"href\":\"/rider/login\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"রেজিস্টার\",\"icon\":\"empty\",\"href\":\"/rider/register\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"ড্যাশবোর্ড\",\"icon\":\"empty\",\"href\":\"/rider/dashboard\",\"target\":\"_self\",\"title\":\"\"}]},{\"text\":\"রেস্তোরাঁ\",\"icon\":\"fas fa-angle-down\",\"href\":\"#\",\"target\":\"_self\",\"title\":\"\",\"children\":[{\"text\":\"লগইন\",\"icon\":\"empty\",\"href\":\"/login\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"রেজিস্টার\",\"icon\":\"empty\",\"href\":\"/restaurant/register\",\"target\":\"_self\",\"title\":\"\"},{\"text\":\"ড্যাশবোর্ড\",\"icon\":\"empty\",\"href\":\"/store/dashboard\",\"target\":\"_self\",\"title\":\"\"}]}]', 'bn', 1, '2020-08-26 11:34:02', '2020-08-26 11:34:02');

-- --------------------------------------------------------

--
-- Table structure for table `meta`
--

CREATE TABLE `meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'excerpt',
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meta`
--

INSERT INTO `meta` (`id`, `term_id`, `type`, `content`, `created_at`, `updated_at`) VALUES
(17, 9, 'preview', 'http://food.chitosewa.com/uploads/dummy/label/label.png', '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(18, 10, 'excerpt', '', '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(19, 10, 'content', '', '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(20, 11, 'excerpt', '', '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(21, 11, 'content', '', '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(22, 12, 'excerpt', '', '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(23, 12, 'content', '', '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(24, 18, 'excerpt', '{\"latitude\":\"27.7172\",\"longitude\":\"85.3240\",\"zoom\":\"12\"}', '2020-08-26 11:55:59', '2020-08-26 21:45:29'),
(25, 18, 'preview', '//food.chitosewa.com/uploads/20/08/26082015984213475f45f963c6f47.jpg', '2020-08-26 11:55:59', '2020-08-26 11:55:59'),
(26, 19, 'excerpt', '{\"latitude\":\"27.4368\",\"longitude\":\"85.0026\",\"zoom\":\"13\"}', '2020-08-26 12:06:55', '2020-08-26 21:47:25'),
(27, 19, 'preview', '//food.chitosewa.com/uploads/20/08/26082015984217645f45fb04b52ef.jpg', '2020-08-26 12:06:55', '2020-08-26 12:06:55'),
(28, 20, 'excerpt', '{\"latitude\":\"26.4525\",\"longitude\":\"87.2718\",\"zoom\":\"13\"}', '2020-08-26 12:10:02', '2020-08-26 21:46:21'),
(29, 20, 'preview', '//food.chitosewa.com/uploads/20/08/26082015984221885f45fcac6bc7b.jpg', '2020-08-26 12:10:02', '2020-08-26 12:10:02'),
(30, 21, 'excerpt', '{\"latitude\":\"26.7271\",\"longitude\":\"85.9407\",\"zoom\":\"13\"}', '2020-08-26 12:28:55', '2020-08-26 21:44:21'),
(31, 21, 'preview', '//food.chitosewa.com/uploads/20/08/26082015984234055f46016d34575.jpg', '2020-08-26 12:28:55', '2020-08-26 12:30:12'),
(32, 22, 'excerpt', '{\"latitude\":\"26.8065\",\"longitude\":\"87.2846\",\"zoom\":\"13\"}', '2020-08-26 16:34:00', '2020-08-26 21:44:10'),
(33, 22, 'preview', '//food.chitosewa.com/uploads/20/08/26082015984380325f463a90815f3.jpg', '2020-08-26 16:34:00', '2020-08-26 16:34:00'),
(34, 24, 'excerpt', 'Veg Pizza', '2020-08-26 23:39:52', '2020-08-26 23:39:52'),
(35, 24, 'preview', '//food.chitosewa.com/uploads/20/08/26082015984428495f464d61d3b60.jpg', '2020-08-26 23:39:52', '2020-08-26 23:39:52'),
(36, 34, 'excerpt', '12\" Veg Piza', '2020-08-26 23:54:59', '2020-08-26 23:54:59'),
(37, 34, 'preview', '//food.chitosewa.com/uploads/20/08/26082015984437855f46510982737.jpg', '2020-08-26 23:54:59', '2020-08-26 23:54:59'),
(38, 35, 'excerpt', '12\" Small Non Veg Pizza', '2020-08-26 23:56:23', '2020-08-26 23:56:23'),
(39, 35, 'preview', '//food.chitosewa.com/uploads/20/08/26082015984438485f465148cc482.jpg', '2020-08-26 23:56:23', '2020-08-26 23:56:23'),
(40, 37, 'excerpt', '8pc Veg. MoMo', '2020-08-27 00:06:41', '2020-08-27 00:06:41'),
(41, 37, 'preview', '//food.chitosewa.com/uploads/20/08/5f4653c5c7fa52608201598444485.png', '2020-08-27 00:06:41', '2020-08-27 00:06:41'),
(42, 38, 'excerpt', '8 Pc Chicken MoMo', '2020-08-27 00:07:39', '2020-08-27 00:07:39'),
(43, 38, 'preview', '//food.chitosewa.com/uploads/20/08/5f4654020887d2608201598444546.png', '2020-08-27 00:07:39', '2020-08-27 00:07:39'),
(44, 39, 'excerpt', 'Pure veg ingredients.', '2020-09-10 17:12:47', '2020-09-10 17:12:47'),
(45, 39, 'preview', '//chitosewa.com/uploads/20/09/10092015997156495f59b941f29f6.jpg', '2020-09-10 17:12:47', '2020-09-10 17:12:47');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_09_12_000000_create_plan_meta_table', 1),
(2, '2014_09_12_000000_create_roles_table', 1),
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2019_08_19_000000_create_failed_jobs_table', 1),
(6, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(7, '2020_02_24_073339_create_medias_table', 1),
(8, '2020_02_24_073655_create_categories_table', 1),
(9, '2020_02_24_094503_create_menu_table', 1),
(10, '2020_04_13_142641_create_options_table', 1),
(11, '2020_04_13_144019_create_terms_table', 1),
(12, '2020_04_13_144038_create_meta_table', 1),
(13, '2020_04_13_144625_create_post_category_table', 1),
(14, '2020_04_16_110534_create_customizers_table', 1),
(15, '2020_06_13_060141_create_user_meta_table', 1),
(16, '2020_06_13_060214_create_shop_day_table', 1),
(17, '2020_06_13_060320_create_locations_table', 1),
(18, '2020_06_13_060350_create_orders_table', 1),
(19, '2020_06_13_060426_create_order_meta_table', 1),
(20, '2020_06_13_060450_create_live_table', 1),
(21, '2020_06_13_060610_create_product_meta_table', 1),
(22, '2020_06_26_095324_create_addons_table', 1),
(23, '2020_06_27_130415_create_featured_user_table', 1),
(24, '2020_06_30_064207_create_user_category_table', 1),
(25, '2020_07_08_113713_create_signal_users_table', 1),
(26, '2020_07_10_152719_create_orderlogs_table', 1),
(27, '2020_07_11_071023_create_riderlogs_table', 1),
(28, '2020_07_13_144449_create_comments_table', 1),
(29, '2020_07_14_105925_create_userplans_table', 1),
(30, '2020_07_15_115854_create_transactions_table', 1),
(31, '2020_08_13_074339_create_comment_meta_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `key`, `value`, `lang`, `created_at`, `updated_at`) VALUES
(1, 'system_basic_info', '{\"number\":\"9801621134\",\"logo\":null}', 'en', '2020-08-26 11:34:00', '2020-08-26 23:26:50'),
(2, 'seo', '{\"title\":\"Chitosewa\",\"description\":\"Food Delivery all Over Nepal\",\"canonical\":\"https:\\/\\/food.chitosewa.com\\/\",\"tags\":\"Nepal Food Delivery\",\"twitterTitle\":\"Chito Sewa\"}', 'en', '2020-08-26 11:34:00', '2020-08-26 21:36:10'),
(3, 'langlist', '{\"English\":\"en\",\"Nepali\":\"ne\"}', 'en', '2020-08-26 11:34:00', '2020-08-26 21:33:32'),
(4, 'lp_imagesize', '[{\"key\":\"small\",\"height\":\"80\",\"width\":\"80\"},{\"key\":\"medium\",\"height\":\"186\",\"width\":\"255\"}]', 'en', '2020-08-26 11:34:00', '2020-08-26 11:34:00'),
(5, 'lp_filesystem', '{\"compress\":\"4\",\"system_type\":\"local\",\"system_url\":null}', 'en', '2020-08-26 11:34:00', '2020-08-26 21:34:45'),
(6, 'lp_perfomances', '{\"lazyload\":0,\"image\":\"uploads\\/lazy.png\"}', 'en', '2020-08-26 11:34:00', '2020-08-26 11:34:00'),
(7, 'rider_commission', '40', 'en', '2020-08-26 11:34:00', '2020-08-26 23:26:50'),
(8, 'currency_name', 'NPR', 'en', '2020-08-26 11:34:01', '2020-08-26 23:26:50'),
(9, 'currency_icon', 'रू', 'en', '2020-08-26 11:34:01', '2020-08-26 23:26:50'),
(10, 'km_rate', '20', 'en', '2020-08-26 11:34:01', '2020-08-26 21:00:21'),
(11, 'default_map', '{\"default_lat\":\"27.7172\",\"default_long\":\"85.3240\",\"default_zoom\":\"12\"}', 'en', '2020-08-26 11:34:01', '2020-08-26 20:39:34'),
(12, 'announcement', '{\"title\":\"Happy to Announcement\",\"message\":\"we testing www.chitosewa.com\\r\\nService Live Very Soon for Service\"}', '1', '2020-08-26 20:36:42', '2020-08-26 21:38:21');

-- --------------------------------------------------------

--
-- Table structure for table `orderlogs`
--

CREATE TABLE `orderlogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orderlogs`
--

INSERT INTO `orderlogs` (`id`, `order_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2020-08-26 20:15:43', '2020-08-26 20:15:43'),
(2, 1, 1, '2020-08-26 20:22:21', '2020-08-26 20:22:21'),
(3, 2, 3, '2020-08-26 21:03:17', '2020-08-26 21:03:17'),
(4, 2, 1, '2020-08-26 21:13:34', '2020-08-26 21:13:34'),
(5, 3, 0, '2020-08-27 12:38:04', '2020-08-27 12:38:04'),
(7, 6, 0, '2020-08-29 22:41:11', '2020-08-29 22:41:11'),
(8, 6, 0, '2020-08-29 22:41:14', '2020-08-29 22:41:14'),
(9, 6, 0, '2020-08-29 22:41:31', '2020-08-29 22:41:31'),
(10, 6, 0, '2020-08-29 22:41:39', '2020-08-29 22:41:39');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` bigint(20) UNSIGNED NOT NULL,
  `rider_id` bigint(20) UNSIGNED DEFAULT NULL,
  `seen` int(11) NOT NULL DEFAULT '0',
  `order_type` int(11) NOT NULL DEFAULT '0',
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `coupon_id` bigint(20) UNSIGNED DEFAULT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `shipping` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `commission` double NOT NULL DEFAULT '0',
  `discount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '2',
  `data` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `vendor_id`, `rider_id`, `seen`, `order_type`, `payment_method`, `payment_status`, `coupon_id`, `total`, `shipping`, `commission`, `discount`, `status`, `data`, `created_at`, `updated_at`) VALUES
(1, 15, 14, 16, 0, 1, 'cod', 1, 23, '2219.00', '284', 443.8, '951.00', 1, '{\"name\":\"SK\",\"phone\":\"9851275055\",\"address\":\"Gwarko-Lamatar Road, Mahalaxmi, Nepal\",\"latitude\":\"27.65974741721379\",\"longitude\":\"85.34372229842529\",\"note\":null}', '2020-08-27 00:12:05', '2020-08-26 20:22:21'),
(2, 15, 14, 16, 0, 1, 'cod', 1, 23, '819.00', '153', 163.8, '351.00', 1, '{\"name\":\"SK\",\"phone\":null,\"address\":\"Gwarko-Lamatar Road, Mahalaxmi, Nepal\",\"latitude\":\"27.660934155505345\",\"longitude\":\"85.34305484388894\",\"note\":null}', '2020-08-26 21:01:34', '2020-08-26 21:13:34'),
(3, 14, 14, NULL, 1, 1, 'cod', 0, NULL, '1570.00', '382', 0, '0.00', 0, '{\"name\":\"Ashish Dulal\",\"phone\":\"9843083493\",\"address\":\"Unnamed Road, Suryabinayak 44800, Nepal\",\"latitude\":\"27.66935158187697\",\"longitude\":\"85.40981250538358\",\"note\":\"as soon as possible\"}', '2020-08-27 12:34:12', '2020-08-27 12:38:04'),
(5, 17, 14, NULL, 0, 1, 'cod', 0, NULL, '1700.00', '224', 0, '0.00', 2, '{\"name\":\"Anil\",\"phone\":\"9898989898\",\"address\":\"Ring Road 640, Kathmandu 44600, Nepal\",\"latitude\":\"27.679110152873744\",\"longitude\":\"85.35055207559958\",\"note\":null}', '2020-08-27 18:37:05', '2020-08-27 18:37:05'),
(6, 17, 14, NULL, 0, 1, 'cod', 0, NULL, '2550.00', '306', 0, '0.00', 0, '{\"name\":\"Anil\",\"phone\":null,\"address\":\"F072, Mahalaxmi, Nepal\",\"latitude\":\"27.6631171\",\"longitude\":\"85.3392405\",\"note\":null}', '2020-08-28 22:03:28', '2020-08-29 22:41:11');

-- --------------------------------------------------------

--
-- Table structure for table `order_meta`
--

CREATE TABLE `order_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL DEFAULT '1',
  `total` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_meta`
--

INSERT INTO `order_meta` (`id`, `order_id`, `term_id`, `qty`, `total`, `created_at`, `updated_at`) VALUES
(1, 1, 24, 2, 850, '2020-08-27 00:12:05', '2020-08-27 00:12:05'),
(2, 1, 34, 1, 870, '2020-08-27 00:12:05', '2020-08-27 00:12:05'),
(3, 1, 25, 1, 200, '2020-08-27 00:12:05', '2020-08-27 00:12:05'),
(4, 1, 29, 1, 125, '2020-08-27 00:12:05', '2020-08-27 00:12:05'),
(5, 1, 33, 1, 100, '2020-08-27 00:12:05', '2020-08-27 00:12:05'),
(6, 1, 38, 1, 125, '2020-08-27 00:12:05', '2020-08-27 00:12:05'),
(7, 1, 36, 1, 50, '2020-08-27 00:12:05', '2020-08-27 00:12:05'),
(8, 2, 34, 1, 870, '2020-08-26 21:01:34', '2020-08-26 21:01:34'),
(9, 2, 25, 1, 200, '2020-08-26 21:01:34', '2020-08-26 21:01:34'),
(10, 2, 30, 1, 100, '2020-08-26 21:01:34', '2020-08-26 21:01:34'),
(11, 3, 34, 1, 870, '2020-08-27 12:34:12', '2020-08-27 12:34:12'),
(12, 3, 26, 1, 500, '2020-08-27 12:34:12', '2020-08-27 12:34:12'),
(13, 3, 25, 1, 200, '2020-08-27 12:34:12', '2020-08-27 12:34:12'),
(17, 5, 24, 2, 850, '2020-08-27 18:37:05', '2020-08-27 18:37:05'),
(18, 6, 24, 3, 850, '2020-08-28 22:03:28', '2020-08-28 22:03:28');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\User', 1, 'token', '700a300393c4fc2a1592c177aaff459e429784950c0d0be89c0c1c74387530f5', '[\"*\"]', '2020-09-10 17:38:38', '2020-09-10 17:22:20', '2020-09-10 17:38:38');

-- --------------------------------------------------------

--
-- Table structure for table `plan_meta`
--

CREATE TABLE `plan_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `s_price` int(11) NOT NULL,
  `duration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `f_resturent` int(11) NOT NULL DEFAULT '0',
  `table_book` int(11) NOT NULL DEFAULT '0',
  `img_limit` int(11) NOT NULL DEFAULT '0',
  `commission` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `plan_meta`
--

INSERT INTO `plan_meta` (`id`, `name`, `s_price`, `duration`, `f_resturent`, `table_book`, `img_limit`, `commission`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Free Membership', 0, 'month', 0, 0, 100, 20, 1, '2020-08-26 11:34:00', '2020-08-27 13:01:24'),
(2, 'Silver Membership', 0, 'month', 0, 1, 250, 25, 0, '2020-08-26 11:34:00', '2020-08-26 12:35:51'),
(3, 'Gold Membership', 0, 'month', 1, 0, 350, 20, 0, '2020-08-26 11:34:00', '2020-08-26 12:35:51'),
(4, 'Platinum Membership', 0, 'month', 1, 1, 500, 13, 0, '2020-08-26 11:34:00', '2020-08-26 12:35:51'),
(5, 'Starter', 1000, 'month', 0, 0, 200, 1000, 1, '2020-08-26 12:32:01', '2020-08-27 12:09:32'),
(6, 'Fast Food', 2000, 'month', 0, 0, 200, 10, 1, '2020-08-26 12:32:33', '2020-08-26 12:39:45'),
(7, 'Lunch', 2000, 'month', 1, 0, 283, 5, 1, '2020-08-26 12:33:35', '2020-08-26 23:18:51'),
(8, 'Dinner', 3000, 'month', 1, 1, 400, 2, 1, '2020-08-26 12:34:23', '2020-08-26 23:19:20'),
(9, 'Bufe', 5000, 'month', 1, 1, 500, 0, 1, '2020-08-26 12:35:15', '2020-08-26 12:35:15');

-- --------------------------------------------------------

--
-- Table structure for table `post_category`
--

CREATE TABLE `post_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_category`
--

INSERT INTO `post_category` (`id`, `term_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 24, 16, '2020-08-26 23:39:52', '2020-08-26 23:39:52'),
(3, 34, 23, '2020-08-26 23:54:59', '2020-08-26 23:54:59'),
(4, 35, 24, '2020-08-26 23:56:23', '2020-08-26 23:56:23'),
(5, 37, 26, '2020-08-27 00:06:41', '2020-08-27 00:06:41'),
(6, 38, 27, '2020-08-27 00:07:39', '2020-08-27 00:07:39'),
(7, 39, 29, '2020-09-10 17:12:48', '2020-09-10 17:12:48');

-- --------------------------------------------------------

--
-- Table structure for table `product_meta`
--

CREATE TABLE `product_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_meta`
--

INSERT INTO `product_meta` (`id`, `term_id`, `price`, `created_at`, `updated_at`) VALUES
(1, 24, 850, '2020-08-26 23:39:52', '2020-08-26 23:39:52'),
(2, 25, 200, '2020-08-26 23:42:44', '2020-08-26 23:42:44'),
(3, 26, 500, '2020-08-26 23:43:24', '2020-08-26 23:43:24'),
(4, 29, 125, '2020-08-26 23:49:13', '2020-08-26 23:49:13'),
(5, 30, 100, '2020-08-26 23:49:42', '2020-08-26 23:49:42'),
(7, 32, 125, '2020-08-26 23:50:34', '2020-08-26 23:50:34'),
(8, 33, 100, '2020-08-26 23:51:39', '2020-08-26 23:51:39'),
(9, 34, 870, '2020-08-26 23:54:59', '2020-08-26 23:54:59'),
(10, 35, 950, '2020-08-26 23:56:23', '2020-08-26 23:56:23'),
(11, 36, 50, '2020-08-27 00:00:03', '2020-08-27 00:00:03'),
(12, 37, 100, '2020-08-27 00:06:41', '2020-08-27 00:06:41'),
(13, 38, 125, '2020-08-27 00:07:39', '2020-08-27 00:07:39'),
(14, 39, 100, '2020-09-10 17:12:48', '2020-09-10 17:12:48');

-- --------------------------------------------------------

--
-- Table structure for table `riderlogs`
--

CREATE TABLE `riderlogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `commision` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `seen` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '2',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `riderlogs`
--

INSERT INTO `riderlogs` (`id`, `user_id`, `order_id`, `commision`, `seen`, `status`, `created_at`, `updated_at`) VALUES
(1, 16, 1, '113.6', 0, 1, '2020-08-26 20:15:43', '2020-08-26 20:22:21'),
(2, 16, 2, '61.2', 1, 1, '2020-08-26 21:03:17', '2020-08-26 21:13:34');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin', '2020-08-26 11:34:00', '2020-08-26 11:34:00'),
(2, 'Author', 'author', '2020-08-26 11:34:00', '2020-08-26 11:34:00'),
(3, 'Store', 'store', '2020-08-26 11:34:00', '2020-08-26 11:34:00'),
(4, 'Rider', 'rider', '2020-08-26 11:34:00', '2020-08-26 11:34:00');

-- --------------------------------------------------------

--
-- Table structure for table `shop_day`
--

CREATE TABLE `shop_day` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `day` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `opening` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `close` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `signal_users`
--

CREATE TABLE `signal_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `player_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en',
  `auth_id` bigint(20) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `terms`
--

INSERT INTO `terms` (`id`, `title`, `slug`, `lang`, `auth_id`, `status`, `type`, `count`, `created_at`, `updated_at`) VALUES
(9, 'Seller Lavel 1', '2300', 'en', 1, 1, 3, 1, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(10, 'terms and conditions', 'terms-and-conditions', 'en', 1, 1, 1, 0, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(11, 'Privacy Policy', 'privacy-policy', 'en', 1, 1, 1, 0, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(12, 'Refund & Return Policy', 'refund-return-policy', 'en', 1, 1, 1, 0, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(18, 'Kathmandu', 'kathmandu', '85.3240', 1, 1, 2, 0, '2020-08-26 11:55:59', '2020-08-26 21:41:14'),
(19, 'hetauda', 'hetauda', '85.0026', 1, 1, 2, 0, '2020-08-26 12:06:55', '2020-08-26 21:44:38'),
(20, 'Biratnagar', 'biratnagar', '87.2718', 1, 1, 2, 0, '2020-08-26 12:10:02', '2020-08-26 21:41:34'),
(21, 'Janakpur', 'janakpur', '85.9407', 1, 1, 2, 0, '2020-08-26 12:28:55', '2020-08-26 12:30:12'),
(22, 'Dharan', 'dharan', '87.2846', 1, 1, 2, 0, '2020-08-26 16:34:00', '2020-08-26 16:36:21'),
(23, 'HAPPY', '2020-09-30', 'en', 14, 1, 10, 30, '2020-08-26 23:32:00', '2020-08-26 23:32:00'),
(24, 'Veg Pizza', 'veg-pizza', 'en', 14, 1, 6, 0, '2020-08-26 23:39:52', '2020-08-26 23:39:52'),
(25, 'Medium (12\")', 'medium', 'en', 14, 1, 8, 0, '2020-08-26 23:42:44', '2020-08-26 23:48:43'),
(26, 'Large (18\")', 'large-18', 'en', 14, 1, 8, 0, '2020-08-26 23:43:24', '2020-08-26 23:43:24'),
(29, 'Extra Chizz', 'extra-chizz', 'en', 14, 1, 8, 0, '2020-08-26 23:49:13', '2020-08-26 23:49:13'),
(30, 'Extra Toping Veg.', 'extra-toping-veg', 'en', 14, 1, 8, 0, '2020-08-26 23:49:42', '2020-08-26 23:49:42'),
(32, 'Extra Topings Chicken', 'extra-topings-chicken', 'en', 14, 1, 8, 0, '2020-08-26 23:50:34', '2020-08-26 23:50:34'),
(33, 'Extra Paneer Topings', 'extra-paneer-topings', 'en', 14, 1, 8, 0, '2020-08-26 23:51:39', '2020-08-26 23:51:39'),
(34, 'Non Veg Pizza', 'non-veg-pizza', 'en', 14, 1, 6, 0, '2020-08-26 23:54:59', '2020-08-26 23:54:59'),
(35, 'Non Veg Pizza', 'non-veg-pizza', 'en', 14, 1, 6, 0, '2020-08-26 23:56:23', '2020-08-26 23:56:23'),
(36, 'Fried', 'fried', 'en', 14, 1, 8, 0, '2020-08-27 00:00:03', '2020-08-27 00:00:03'),
(37, 'Veg MoMO', 'veg-momo', 'en', 14, 1, 6, 0, '2020-08-27 00:06:41', '2020-08-27 00:06:41'),
(38, 'Chicken MoMo', 'chicken-momo', 'en', 14, 1, 6, 0, '2020-08-27 00:07:39', '2020-08-27 00:07:39'),
(39, 'veg momo', 'veg-momo', 'en', 18, 1, 6, 0, '2020-09-10 17:12:47', '2020-09-10 17:12:47');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `admin_id` bigint(20) UNSIGNED DEFAULT NULL,
  `amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_mode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `user_id`, `admin_id`, `amount`, `payment_mode`, `status`, `created_at`, `updated_at`) VALUES
(1, 16, NULL, '113.60', 'bank-transfer', 2, '2020-08-26 20:23:04', '2020-08-26 20:23:04'),
(2, 14, 1, '2219', 'bank-transfer', 1, '2020-08-26 20:38:56', '2020-08-26 20:46:38');

-- --------------------------------------------------------

--
-- Table structure for table `userplans`
--

CREATE TABLE `userplans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `plan_id` bigint(20) UNSIGNED NOT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `amount` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `badge_id` bigint(20) UNSIGNED DEFAULT NULL,
  `plan_id` bigint(20) UNSIGNED DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'http://food.chitosewa.com/uploads/store.jpg',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `badge_id`, `plan_id`, `slug`, `avatar`, `name`, `email`, `status`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL, 'admin', 'http://food.chitosewa.com/uploads/store.jpg', 'Admin', 'admin@admin.com', 'pending', NULL, '$2y$10$zmuS6q9XCVmcSbe/814i/ehzdwpotZFsODsLCGQcoIIgmtzWFxeW.', NULL, '2020-08-26 11:34:00', '2020-09-10 17:29:46'),
(2, 2, NULL, NULL, 'author', 'http://food.chitosewa.com/uploads/store.jpg', 'Author', 'author@author.com', 'pending', NULL, '$2y$10$qED5IAXnz7/lZpOkn7Hk0eqvYbVMx4Q73eagCR22NkeyWYiM..RVi', NULL, '2020-08-26 11:34:01', '2020-08-26 11:34:01'),
(12, 4, NULL, NULL, 'rider', 'http://food.chitosewa.com/uploads/store.jpg', 'Rider', 'rider@email.com', 'approved', '2020-08-03 04:11:23', '$2y$10$/NGXQBcof5SBY3EPxE34/uxOamOGWX94oyxKevp9tunv1Hup7F9x6', NULL, '2020-08-26 11:34:02', '2020-08-26 11:34:02'),
(13, 3, 9, 6, 'ashish-food', 'http://food.chitosewa.com/uploads/store.jpg', 'Ashish Food', 'azizdulal.ad@gmail.com', 'approved', '2020-08-26 23:14:10', '$2y$10$Bc49VRQnu2Kt1WwNs8hpXOse80Fk5i00bNDu7QokNfamUJG3N24S6', NULL, '2020-08-26 11:51:30', '2020-08-27 13:04:33'),
(14, 3, 9, 1, 'nbtc-food', 'http://food.chitosewa.com/uploads/store.jpg', 'NBTC FOOD', 'nbtcfood@chiotosewa.com', 'approved', '2020-08-26 23:14:05', '$2y$10$8bK230pgH/.nXJKxK3b7VOGE2bhzBdCwqF8B9MtzZN3madqRdhE.G', NULL, '2020-08-26 22:54:58', '2020-08-26 21:28:46'),
(15, 2, NULL, NULL, 'sk', 'http://food.chitosewa.com/uploads/store.jpg', 'SK', 'nepshopy@gmail.com', 'approved', NULL, '$2y$10$X/Vq914HgmzwFu78H364cOofsS6Q.CFTbImxGWeFRIS83AUJ1uytO', 'm5gaGO59aFbRdOpwSdlS5RoLi0d2apk2wocY8iRvc7lgBLO5Fijm9t5pUh1Z', '2020-08-27 00:09:49', '2020-08-26 20:18:44'),
(16, 4, 9, NULL, 'test-rider', 'http://food.chitosewa.com/uploads/store.jpg', 'Test Rider 1', 'dreamearth.net@gmail.com', 'offline', '2020-08-27 00:30:36', '$2y$10$zmuS6q9XCVmcSbe/814i/ehzdwpotZFsODsLCGQcoIIgmtzWFxeW.', 'nY9t1D1B4F0huIL5bjgy8hzJv7xG16Ya4VhhfArPSR120GMRQfVIi4GHuvJm', '2020-08-27 00:14:08', '2020-09-10 17:45:13'),
(17, 2, NULL, NULL, 'anil', 'http://food.chitosewa.com/uploads/store.jpg', 'Anil', 'richatravels34@gmail.com', 'pending', NULL, '$2y$10$.C12U0HlDhHsBoV2ygMmY.BRYjKC4267TB5pLyRQOsOl2A.vZVF/2', '6a2JD05yT9Bv1ZfD1baeJwx6XjdYzSR5fDzNvUMWG6iSIaZiCChVFR6WTqxm', '2020-08-27 18:33:48', '2020-08-27 18:33:48'),
(18, 3, 9, 1, 'chito-khana', 'http://food.chitosewa.com/uploads/store.jpg', 'Chito Khana', 'dulalashish052@gmail.com', 'approved', '2020-09-10 16:51:03', '$2y$10$eex3k4kaovkqsmIUmgxW3edPC9UDDLvuVG/u69up0wvZwd/w5gfw2', NULL, '2020-09-10 16:45:32', '2020-09-10 16:53:49');

-- --------------------------------------------------------

--
-- Table structure for table `user_category`
--

CREATE TABLE `user_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_meta`
--

CREATE TABLE `user_meta` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'preview',
  `content` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_meta`
--

INSERT INTO `user_meta` (`id`, `user_id`, `type`, `content`, `created_at`, `updated_at`) VALUES
(64, 12, 'info', '{\"phone1\":\"+1 (157) 364-3678\",\"phone2\":\"+1 (696) 983-9689\",\"full_address\":\"Agrabad, Chattogram, Bangladesh\"}', '2020-08-26 11:34:02', '2020-08-26 11:34:02'),
(65, 13, 'delivery', '30', '2020-08-26 11:51:30', '2020-08-26 11:51:30'),
(66, 13, 'pickup', '30', '2020-08-26 11:51:30', '2020-08-26 11:51:30'),
(67, 13, 'rattings', '0', '2020-08-26 11:51:30', '2020-08-26 11:51:30'),
(68, 13, 'avg_rattings', '0', '2020-08-26 11:51:30', '2020-08-26 11:51:30'),
(69, 13, 'info', '{\"description\":\"Hello Welcome to Ashish Food\",\"phone1\":\"9843083493\",\"phone2\":\"9843083493\",\"email1\":\"azizdulal.ad@gmail.com\",\"email2\":\"dulalashish@gmail.com\",\"address_line\":\"koteshwor\",\"full_address\":\"balkumari\"}', '2020-08-26 11:51:30', '2020-08-26 12:23:55'),
(70, 13, 'gallery', NULL, '2020-08-26 11:51:30', '2020-08-26 11:51:30'),
(71, 13, 'preview', NULL, '2020-08-26 11:51:30', '2020-08-26 11:51:30'),
(72, 14, 'delivery', '45', '2020-08-26 22:54:58', '2020-08-26 22:54:58'),
(73, 14, 'pickup', '30', '2020-08-26 22:54:58', '2020-08-26 22:54:58'),
(74, 14, 'rattings', '2', '2020-08-26 22:54:58', '2020-09-10 17:22:41'),
(75, 14, 'avg_rattings', '4.5', '2020-08-26 22:54:58', '2020-09-10 17:22:41'),
(76, 14, 'info', '{\"description\":\"NBTC FOOD\",\"phone1\":\"9801621134\",\"phone2\":\"9851275055\",\"email1\":\"nbtcfood@chitosewa.com\",\"email2\":\"nbtcfood@chitosewa.com\",\"address_line\":\"NBTC, Kakanki\",\"full_address\":\"Ring Road, Kathmandu 44618, Nepal\"}', '2020-08-26 22:54:58', '2020-08-26 20:49:54'),
(77, 14, 'gallery', NULL, '2020-08-26 22:54:58', '2020-08-26 22:54:58'),
(78, 14, 'preview', '//food.chitosewa.com/uploads/20/08/5f46766b98dbb2608201598453355.png', '2020-08-26 22:54:58', '2020-08-26 20:50:14'),
(79, 16, 'info', '{\"phone1\":\"9823867340\",\"phone2\":\"9823867340\",\"full_address\":\"Kupondole, Lalitpur, Nepal\"}', '2020-08-27 00:14:08', '2020-08-26 21:08:00'),
(80, 14, 'livechat', NULL, '2020-08-26 20:29:12', '2020-08-26 20:29:12'),
(81, 18, 'delivery', '60', '2020-09-10 16:45:32', '2020-09-10 16:45:32'),
(82, 18, 'pickup', '30', '2020-09-10 16:45:32', '2020-09-10 16:45:32'),
(83, 18, 'rattings', '0', '2020-09-10 16:45:32', '2020-09-10 16:45:32'),
(84, 18, 'avg_rattings', '0', '2020-09-10 16:45:32', '2020-09-10 16:45:32'),
(85, 18, 'info', '{\"description\":\"All Kinds of food and groceries.\",\"phone1\":\"9843083493\",\"phone2\":\"9813312456\",\"email1\":\"azizdulal.ad@gmail.com\",\"email2\":\"ashishdulal052@gmail.com\",\"address_line\":\"koteshwor\",\"full_address\":\"Koteshwor Chowk \\u0915\\u093e\\u0947\\u091f\\u0947\\u0936\\u094d\\u0935\\u0930 \\u091a\\u093e\\u0947\\u0915, Ring Road, Kathmandu, Nepal\"}', '2020-09-10 16:45:32', '2020-09-10 17:42:37'),
(86, 18, 'gallery', NULL, '2020-09-10 16:45:32', '2020-09-10 16:45:32'),
(87, 18, 'preview', NULL, '2020-09-10 16:45:32', '2020-09-10 16:45:32'),
(88, 18, 'livechat', NULL, '2020-09-10 17:41:28', '2020-09-10 17:41:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addons`
--
ALTER TABLE `addons`
  ADD KEY `addons_term_id_foreign` (`term_id`),
  ADD KEY `addons_addon_id_foreign` (`addon_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_vendor_id_foreign` (`vendor_id`),
  ADD KEY `comments_order_id_foreign` (`order_id`);

--
-- Indexes for table `comment_meta`
--
ALTER TABLE `comment_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comment_meta_comment_id_foreign` (`comment_id`),
  ADD KEY `comment_meta_vendor_id_foreign` (`vendor_id`);

--
-- Indexes for table `customizers`
--
ALTER TABLE `customizers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `featured_user`
--
ALTER TABLE `featured_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `featured_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `live`
--
ALTER TABLE `live`
  ADD PRIMARY KEY (`id`),
  ADD KEY `live_order_id_foreign` (`order_id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `locations_user_id_foreign` (`user_id`),
  ADD KEY `locations_role_id_foreign` (`role_id`),
  ADD KEY `locations_term_id_foreign` (`term_id`);

--
-- Indexes for table `medias`
--
ALTER TABLE `medias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `medias_user_id_foreign` (`user_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `meta`
--
ALTER TABLE `meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `meta_term_id_foreign` (`term_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderlogs`
--
ALTER TABLE `orderlogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orderlogs_order_id_foreign` (`order_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`);

--
-- Indexes for table `order_meta`
--
ALTER TABLE `order_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_meta_order_id_foreign` (`order_id`),
  ADD KEY `order_meta_term_id_foreign` (`term_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `plan_meta`
--
ALTER TABLE `plan_meta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_category`
--
ALTER TABLE `post_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_category_term_id_foreign` (`term_id`),
  ADD KEY `post_category_category_id_foreign` (`category_id`);

--
-- Indexes for table `product_meta`
--
ALTER TABLE `product_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_meta_term_id_foreign` (`term_id`);

--
-- Indexes for table `riderlogs`
--
ALTER TABLE `riderlogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `riderlogs_order_id_foreign` (`order_id`),
  ADD KEY `riderlogs_user_id_foreign` (`user_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_day`
--
ALTER TABLE `shop_day`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shop_day_user_id_foreign` (`user_id`);

--
-- Indexes for table `signal_users`
--
ALTER TABLE `signal_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `signal_users_user_id_foreign` (`user_id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `terms_auth_id_foreign` (`auth_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_user_id_foreign` (`user_id`);

--
-- Indexes for table `userplans`
--
ALTER TABLE `userplans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userplans_user_id_foreign` (`user_id`),
  ADD KEY `userplans_plan_id_foreign` (`plan_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`),
  ADD KEY `users_plan_id_foreign` (`plan_id`);

--
-- Indexes for table `user_category`
--
ALTER TABLE `user_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_category_user_id_foreign` (`user_id`),
  ADD KEY `user_category_category_id_foreign` (`category_id`);

--
-- Indexes for table `user_meta`
--
ALTER TABLE `user_meta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_meta_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `comment_meta`
--
ALTER TABLE `comment_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `customizers`
--
ALTER TABLE `customizers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `featured_user`
--
ALTER TABLE `featured_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `live`
--
ALTER TABLE `live`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `medias`
--
ALTER TABLE `medias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `meta`
--
ALTER TABLE `meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `orderlogs`
--
ALTER TABLE `orderlogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `order_meta`
--
ALTER TABLE `order_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `plan_meta`
--
ALTER TABLE `plan_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `post_category`
--
ALTER TABLE `post_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product_meta`
--
ALTER TABLE `product_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `riderlogs`
--
ALTER TABLE `riderlogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `shop_day`
--
ALTER TABLE `shop_day`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `signal_users`
--
ALTER TABLE `signal_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `userplans`
--
ALTER TABLE `userplans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `user_category`
--
ALTER TABLE `user_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `user_meta`
--
ALTER TABLE `user_meta`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addons`
--
ALTER TABLE `addons`
  ADD CONSTRAINT `addons_addon_id_foreign` FOREIGN KEY (`addon_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `addons_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `comment_meta`
--
ALTER TABLE `comment_meta`
  ADD CONSTRAINT `comment_meta_comment_id_foreign` FOREIGN KEY (`comment_id`) REFERENCES `comments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comment_meta_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `featured_user`
--
ALTER TABLE `featured_user`
  ADD CONSTRAINT `featured_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `live`
--
ALTER TABLE `live`
  ADD CONSTRAINT `live_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `locations`
--
ALTER TABLE `locations`
  ADD CONSTRAINT `locations_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `locations_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `locations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `medias`
--
ALTER TABLE `medias`
  ADD CONSTRAINT `medias_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `meta`
--
ALTER TABLE `meta`
  ADD CONSTRAINT `meta_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orderlogs`
--
ALTER TABLE `orderlogs`
  ADD CONSTRAINT `orderlogs_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_meta`
--
ALTER TABLE `order_meta`
  ADD CONSTRAINT `order_meta_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_meta_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_category`
--
ALTER TABLE `post_category`
  ADD CONSTRAINT `post_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `post_category_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_meta`
--
ALTER TABLE `product_meta`
  ADD CONSTRAINT `product_meta_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `riderlogs`
--
ALTER TABLE `riderlogs`
  ADD CONSTRAINT `riderlogs_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `riderlogs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `shop_day`
--
ALTER TABLE `shop_day`
  ADD CONSTRAINT `shop_day_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `signal_users`
--
ALTER TABLE `signal_users`
  ADD CONSTRAINT `signal_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `terms`
--
ALTER TABLE `terms`
  ADD CONSTRAINT `terms_auth_id_foreign` FOREIGN KEY (`auth_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `userplans`
--
ALTER TABLE `userplans`
  ADD CONSTRAINT `userplans_plan_id_foreign` FOREIGN KEY (`plan_id`) REFERENCES `plan_meta` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `userplans_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_plan_id_foreign` FOREIGN KEY (`plan_id`) REFERENCES `plan_meta` (`id`),
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_category`
--
ALTER TABLE `user_category`
  ADD CONSTRAINT `user_category_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_category_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_meta`
--
ALTER TABLE `user_meta`
  ADD CONSTRAINT `user_meta_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
