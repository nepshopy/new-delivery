<style type="text/css">
    .carousel-caption p {
    color: black;
    font-weight: 600;
    font-size: x-large;
    display: none;
}
@media only screen and (max-width: 768px){
    .display-tablet{
    flex: 100%;
    max-width: 100%;
    margin-bottom: 0 !important;
}
.my-responsive-height{
    height: auto;
}
.details-head{
    padding-top: 2rem !important;
    padding-bottom: 0 !important;
}
.details-head div h1, .details-head div p {
    display: none !important;
}
}
</style>
<section class="section-profile-cover section-shaped my-0 d-block d-md-block d-lg-block d-lx-block my-responsive-height">
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-6"></div>
        <div style="z-index: 99;" class="col-xl-6 col-lg-6 col-md-6 my-7 display-tablet">
            <!-- Circles background -->
            <!-- <img class="bg-image" src="{{ config('global.search') }}" style="width: 100%;"> -->
            <!--Carousel Wrapper-->
            <div id="chitosewa-carousel" class="carousel slide carousel-fade" data-ride="carousel">
              <!--Indicators-->
              <ol class="carousel-indicators">
                <li data-target="#chitosewa-carousel" data-slide-to="0" class="active"></li>
                <li data-target="#chitosewa-carousel" data-slide-to="1"></li>
                <li data-target="#chitosewa-carousel" data-slide-to="2"></li>
            </ol>
            <!--/.Indicators-->
            <!--Slides-->
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active">
                  <div class="view">
                    <a href="/">
                    <img class="d-block w-100" src="{{asset('/default/0.jpg')}}" alt="shop at home">
                    </a>
                    <div class="mask rgba-black-light"></div>
                </div>
                <div class="carousel-caption">
                    <p>Enjoy Shopping at Home</p>
                </div>
            </div>
                <div class="carousel-item">
                  <div class="view">
                    <a href="/">
                    <img class="d-block w-100" src="{{asset('/default/shopping.jpg')}}" alt="shop at home">
                    </a>
                    <div class="mask rgba-black-light"></div>
                </div>
                <div class="carousel-caption">
                    <p>Shop at Home</p>
                </div>
            </div>
            <div class="carousel-item">
              <!--Mask color-->
              <div class="view">
                    <a href="/">
                <img class="d-block w-100" src="{{asset('/default/myfoods.jpg')}}" alt="Foods at your place">
                    </a>
                <div class="mask rgba-black-strong"></div>
            </div>
            <div class="carousel-caption">
                <p>Foods at your place</p>
            </div>
        </div>
        <div class="carousel-item">
          <!--Mask color-->
          <div class="view">
                    <a href="/">
                <img class="d-block w-100" src="{{asset('/default/foods.jpg')}}" alt="Being Foody">
            </a>
            <div class="mask rgba-black-slight"></div>
        </div>
        <div class="carousel-caption">
            <p>Being Foody</p>
        </div>
    </div>
</div>
<!--/.Slides-->
<!--Controls-->
<a class="carousel-control-prev" href="#chitosewa-carousel" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
</a>
<a class="carousel-control-next" href="#chitosewa-carousel" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
</a>
<!--/.Controls-->
</div>
<!--/.Carousel Wrapper-->
</div>
</div>
</section>
<section class="section details-head">
    <div class="container mt--350">
        <h1><?php echo config('global.header_title') ?></h1>
        <p><?php echo config('global.header_subtitle') ?></p>
        @if(env('is_demo', false))
        <div class="row">
            <div class=""><div class="blob red"></div></div>
            <div class=""> <span class="description "><strong>Demo info</strong>: Our demo restorants deliver in: <a href="?location=Bronx,NY,USA">Bronx</a>, <a href="?location=Manhattan, New York, NY, USA">Manhattn</a>, <a href="?location=Queens, New York, NY, USA">Queens</a>, <a href="?location=Brooklyn, New York, NY, USA">Brooklyn</a></span></div>
        </div>
        <br />
        @endif
        <div class="row">
            <div class="col-md-4">
                @if(env('ENABLE_LOCATION_SEARCH',false))
                <form action="{{ route('front') }}">
                    <div class="form-group{{ $errors->has('location') ? ' has-danger' : '' }}">
                        <div class="input-group mb-4">
                            <input class="form-control" name="location" id="txtlocation" value="{{ $lastaddress }}" placeholder="{{ __('Enter your street or address') }}" type="text" required>
                            <div type="button" class="input-group-append button">
                                <span class="input-group-text"><i id="search_location" class="fa fa-map-pin" data-toggle="tooltip" data-placement="top" title="{{ __('Get my current location')}}"></i></span>
                            </div>
                            @if ($errors->has('location'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('location') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                </div>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-danger">{{ __('Find restaurants') }}</button>
                </div>
            </form>



            @else

            <form>
                <div class="form-group">
                    <div class="input-group mb-4">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="ni ni-basket"></i></span>
                        </div>
                        <input name="q" class="form-control lg" value="{{ request()->get('q') }}" placeholder="{{ __ ('Search') }}" type="text">
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <button type="submit" class="btn btn-danger">{{ __('Find your meal') }}</button>
            </div>
        </form>
        @endif
    </div>
</div>
</section>
